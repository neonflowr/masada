"use strict";

import { getPagedSongsInfo, getAllSongsInfo } from "./modules/song.js";
import trackItemComponent from "./components/track_item.js";
import pageItemComponent from "./components/pagination.js";
import { microCommentItemComponent } from "./components/comment_item.js";
import progressBarComponent from "./components/progress_bar.js";
import * as HttpHandler from "./modules/http.js";

import { i18n } from "./modules/locale.js";

let max_videos_in_one_page = 42;
let initial_offset = 0;
let current_page_number = 1;

let popular_songs_data;

let search_initialized = false;
let autocom;

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});



let app = Vue.createApp({
    components: {
        "track-item": trackItemComponent,
        "page-item": pageItemComponent,
        "comment-item": microCommentItemComponent,
        "progress-bar": progressBarComponent
    },
    data() {
        return {
            popular_uploads_list: [],
            uploads_list: [],
            is_empty: false,
            comments_list: [],
            is_comments_empty: false,
            is_popular_list_visible: false,
            pages: [],
            featured_post_info: {
                img_src: "",
                description: "",
                posts: []
            },
            banner_img_info: {
                path: null,
                artist_name: null,
                game_name: null,
            },
        };
    },
    async mounted() {
		initSearchBox();
        this.$refs.progressbar.setStartedStatus();

        i18n.global.locale = HttpHandler.setLangFromCookie();


		{
			let urlParams = new URLSearchParams(window.location.search);
			let offset_query = urlParams.get("offset");

			if (offset_query) initial_offset = Number(offset_query) > 0 ? Number(offset_query) : 0;

			if (initial_offset !== 0) {
				current_page_number += Math.ceil(initial_offset / max_videos_in_one_page);
			}

			const data = await getPagedSongsInfo(initial_offset);

			const total_size = data["total_size"];

			let page_count = Math.ceil(total_size / max_videos_in_one_page);

			populateVideoGrid(data["items"], data["result_size"]);

			for (let i = 0; i < page_count; i++) {
				this.pages.push({
					text: i + 1,
					href: i === 0 ? "/" : "?offset=" + i * max_videos_in_one_page,
					isActive: false,
					isDisabled: false,
					onclick: async function (event) {
						event.preventDefault();

						const data = await getPagedSongsInfo(i * max_videos_in_one_page);
						populateVideoGrid(data["items"], data["result_size"]);

						setCurrentPage(i + 1);
						location.href = "#newest";
					}
				});
			}

			this.pages.unshift({
				text: this.$t("message.homepage_previous"),
				href: "?offset=" + (current_page_number < 2 ? 0 : current_page_number - 2) * max_videos_in_one_page,
				isActive: false,
				isDisabled: current_page_number === 1,
				onclick: async function (event) {
					event.preventDefault();
					if (this.$el.classList.contains("disabled")) return;

					const data = await getPagedSongsInfo((current_page_number < 2 ? 0 : current_page_number - 2) * max_videos_in_one_page);
					populateVideoGrid(data["items"], data["result_size"]);
					setCurrentPage(current_page_number - 1);
					location.href = "#newest";
				}
			});

			this.pages.push({
				text: this.$t("message.homepage_next"),
				href: "?offset=" + (current_page_number) * max_videos_in_one_page,
				isActive: false,
				isDisabled: current_page_number === page_count,
				onclick: async function (event) {
					event.preventDefault();
					if (this.$el.classList.contains("disabled")) return;
					const data = await getPagedSongsInfo((current_page_number) * max_videos_in_one_page);
					populateVideoGrid(data["items"], data["result_size"]);
					setCurrentPage(current_page_number + 1);
					location.href = "#newest";
				}
			});
		}

		{
			this.pages[current_page_number].isActive = true;

			if (current_page_number !== 1) {
				window.history.replaceState({}, "", `?offset=${(current_page_number - 1) * max_videos_in_one_page}`);
			} else {
				window.history.replaceState({}, "", "/");
			}
		}

		//     HttpHandler.getFeaturedPost().then(res => {
		//         if (res.error) {
		//             console.error(res.error);
		//             return;
		//         }

		//         if (!res.data) return;

		//         let full_posts_info = [];
		//         res.data.posts.forEach(val => {
		//             let post_info = all_songs_info.find(i => i._id === val);
		//             if (post_info) full_posts_info.push(post_info);
		//         });
		//         res.data.posts = full_posts_info;

		//         this.featured_post_info = res.data;
		//     });

		//     this.$refs.progressbar.setFinishedStatus();
		// });

		HttpHandler.getAsync(HttpHandler.HOMEPAGE_LISTENER_INFO_URL).then((data) => {
			popular_songs_data = JSON.parse(data.responseText);
			if (popular_songs_data) popular_songs_data = popular_songs_data["items"];
			addPopularSongsToGrid();
		}).catch(err => {
			console.error(err);
			showToastMessage("something went wrong... try refreshing the page.");
			this.$refs.progressbar.setFinishedStatus();
		});

		HttpHandler.getAllPostsWithComments().then(result => {
			if (result.error) {
				showToastMessage("Could not get comments...");
			} else {
				// Sort the posts chronologically
				// Really should have set a separate comments collection huh...
				let sorted_posts = [];
				result.data.forEach(post => {
					sorted_posts.push({
						most_recent_comment: post.comments[post.comments.length - 1],
						post: post
					});
				});

				let i, j;
				for (i = 0; i < sorted_posts.length - 1; i++) {
					for (j = 0; j < sorted_posts.length - i - 1; j++) {
						if (sorted_posts[j].most_recent_comment.timestamp > sorted_posts[j + 1].most_recent_comment.timestamp) {
							let temp = sorted_posts[j];
							sorted_posts[j] = sorted_posts[j + 1];
							sorted_posts[j + 1] = temp;
						}
					}
				}

				let final = [];
				sorted_posts.forEach(val => {
					final.push({
						info: val.most_recent_comment,
						post: val.post
					});
				});
				final.reverse();

				this.comments_list = final;

				this.is_comments_empty = this.comments_list.length < 1;
			}

			this.$refs.progressbar.setFinishedStatus();
		});
	}
});

app.config.globalProperties.window = window;
app.use(i18n);
app = app.mount("#app");

function populateVideoGrid(data, size) {
	app.uploads_list = [];

	for (let i = 0; i < size; i++) {
		app.uploads_list.push(data[i]);
	}

	app.is_empty = app.uploads_list.length === 0;
}

function initSearchBox() {
	autocom = new autoComplete({
		data: {
			src: null,
			keys: ["search_keywords"],
			cache: true,
		},
		selector: "#search-box",
		resultItem: {
			element: (item, data) => {
				item.innerHTML = data.value.name;
				item.innerText = data.value.name;
			},
		},
		resultsList: {
			maxResults: 20,
		},
	});

	document.getElementById("search-box").addEventListener("click", (event) => {
		if (!search_initialized) {
			getAllSongsInfo().then(result => {
				autocom.data.src = result;
				search_initialized = true;
			});
		}
	});

	document.getElementById("search-box").addEventListener("selection", (event) => {
		window.location.replace("/?track_id=" + event.detail.selection.value._id);
	});
}

function showToastMessage(message) {
	toastList[0].show();
	toastElList[0].querySelector(".toast-body > p").innerText = message;
}

function setCurrentPage(number) {
	app.pages.forEach(val => val.isActive = false);
	app.pages[number].isActive = true;

	current_page_number = number;

	if (number !== 1) {
		window.history.pushState({}, "", `?offset=${(number - 1) * max_videos_in_one_page}`);
		app.pages[0].isDisabled = false;

		app.pages[0].href = "?offset=" + (number < 2 ? 0 : number - 2) * max_videos_in_one_page;

		if (number === app.pages.length - 2) {
			app.pages[app.pages.length - 1].isDisabled = true;
		} else {
			app.pages[app.pages.length - 1].isDisabled = false;
		}
		app.pages[app.pages.length - 1].href = "?offset=" + (number) * max_videos_in_one_page;
	} else {
		window.history.pushState({}, "", "/");
		app.pages[0].isDisabled = true;
		app.pages[0].href = "/";

		app.pages[app.pages.length - 1].href = "?offset=" + (number) * max_videos_in_one_page;
	}
}

function addPopularSongsToGrid() {
	try {
		let list = [];

		Object.keys(popular_songs_data).forEach(result => {
			list.push(popular_songs_data[result]);
		});

		let i, j;
		for (i = 0; i < list.length - 1; i++) {
			for (j = 0; j < list.length - i - 1; j++) {
				if (list[j].listeners.length < list[j + 1].listeners.length) {
					let temp = list[j];
					list[j] = list[j + 1];
					list[j + 1] = temp;
				}
			}
		}

		list.forEach(val => {
			app.popular_uploads_list.push(val);
		});

		app.is_popular_list_visible = app.popular_uploads_list.length > 0;
	} catch (err) {
		console.error(err, "Could not add popular songs");
	}
}
