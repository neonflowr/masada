import * as HttpHandler from "./modules/http.js";
import { i18n } from "./modules/locale.js";
import userCardComponent from "./components/user_card.js";

let app = Vue.createApp({
    components: {
        "user-card": userCardComponent
    },
    data() {
        return {
            users: [],
            count: 0,
            is_empty: false
        };
    },
    mounted() {
        i18n.global.locale = HttpHandler.setLangFromCookie();
    }
});
app.use(i18n);
app = app.mount("#app");

HttpHandler.getAsync(HttpHandler.USER_INFO_URL).then((info) => {
    if (info && info.status === 200) {
        let data = JSON.parse(info.responseText);

        app.count = data.length;

        data.forEach(val => {
            val.relative_registered_on = moment(val.registered_on).fromNow();
        });

        app.users = data.reverse();

        app.is_empty = app.users.length < 1;
    }
});
