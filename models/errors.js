class Error {
    message;

    constructor(message) {
        this.message = message ?? "";
    }
}

class TrackUploadedError extends Error {}

class ProcessingError extends Error {}

class NotFoundError extends Error {}

class ResourceAlreadyExistError extends Error {}

class InvalidParameterError extends Error {}

class ValidationError extends Error {}

export { Error, TrackUploadedError, ProcessingError, NotFoundError, InvalidParameterError, ValidationError, ResourceAlreadyExistError };
