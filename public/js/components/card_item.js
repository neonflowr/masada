const cardItemComponent = {
    props: ["title", "href", "img_src", "desc"],
    template: `
        <div class="col-4 col-sm-2 track-item p-3" v-bind:title="title">
            <div class="track-item-container h-100 w-100">
                <a v-bind:href="href" class="h-100 w-100" >
                    <img class="img-fluid" v-bind:src="img_src" loading="lazy" />
                    <p class="pt-2 mx-3 mw-100 track-item-title"> {{ title }} </p>
                    <p class="mx-3 track-item-description" style="white-space: nowrap;"> {{ desc }} </p>
                </a>
            </div>
        </div>
    `
};

export default cardItemComponent;