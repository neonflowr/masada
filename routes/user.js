import express from "express";
import { connection, mongodb } from "../func/db.js";
import { authenticate, authenticateUser } from "../func/auth_helper.js";
import { BASE_PUBLIC_PATH, upload, fileHandler, BASE_LOCAL_PATH } from "../func/file.js";
import bcrypt from "bcrypt";
import { BG_INFO_404, getWallpaper } from "./image.js";

import { UserRoles, UserModel, DEFAULT_AVATAR_URL, MicroUser } from "../models/user.js";

import { getListenersInfo } from "../func/chat.js";

import { track_collection, TRACK_COLLECTION_NAME } from "./track.js";

import * as Errors from "../models/errors.js";

import { dirname } from "path";

import { replaceUnderscoreWithSpace, getRangedRandomIndex, formatBytes } from "../func/utils.js";

export const userRoutes = express.Router();

export let user_collection;

const SALT_ROUNDS = 10;

connection.then((db_connection) => {
    db_connection.collection("user", (err, result) => {
        if (err) {
            console.error(`${err}\nCould not get user data from database`);
            return;
        }

        user_collection = result;
    });
});

userRoutes.post("/login", (req, res) => {
    user_collection.findOne({ username: req.body.username }).then(result => {
        if (result) {
            bcrypt.compare(req.body.password, result.password, (err, compare_result) => {
                if (compare_result) {
                    // Successful login
                    createUserSession(req, result, req.body.remember_me);
                    res.send({
                        error: null
                    });
                } else {
                    res.send({
                        error: new Errors.InvalidParameterError(req.__("login_wrong_password"))
                    });
                }
            });
        } else {
            res.send({
                error: new Errors.NotFoundError(req.__("login_user_not_exist", {name: req.body.username}))
            });
        }
    }).catch(err => {
        console.error(err, new Errors.ProcessingError(`Could not retrieve data for user ${req.body.username}`));
        res.send({
            error: new Errors.ProcessingError(req.__("login_user_data_fail"))
        });
    });
});

userRoutes.post("/register", (req, res) => {
    bcrypt.genSalt(SALT_ROUNDS, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
            user_collection.find({ username: req.body.username }).toArray((err, result) => {
                if (result.length === 0) {
                    fileHandler.readDir(BASE_LOCAL_PATH + "/avatars/default", (err, files) => {
                        if (err) {
                            console.error(`${err}\nCould not access default avatar directory`);
                            res.status(500).end();
                            return;
                        }

                        let default_avatar_file_path = DEFAULT_AVATAR_URL + files[getRangedRandomIndex(files.length)];

                        user_collection.find({}).toArray((err, result) => {
                            let user = new UserModel(null, req.body.username, hash, req.body.email, default_avatar_file_path, result.length > 0 ? UserRoles.NORMAL : UserRoles.ADMIN, new Date());

                            user_collection.insertOne(user, () => {
                                createUserSession(req, user, true);

                                res.send({
                                    error: null
                                });
                            });
                        });
                    });
                } else {
                    res.send({
                        error: new Errors.InvalidParameterError(req.__("register_user_exists", { name: req.body.username }))
                    });
                }
            });
        });
    });
});

userRoutes.get("/all", (req, res) => {
    authenticate(req, () => {
        console.log("Received request to get all users' info");

        user_collection.find({}).toArray((err_array, result) => {
            if (err_array) {
                console.error(err_array, "Failed to process all users' info");
                res.status(500).end();
                return;
            }

            // Hide sensitive data
            result.forEach((val, index) => {
                result[index] = _clearPrivateFields(val);
            });

            res.send(result);
        });
    }, () => {
        res.status(401).send({
            error: new Errors.ValidationError("Unauthenticated request")
        });
    });
});

userRoutes.get("/logout", (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            console.error(`${err}\nFailed to process logout request`);
        } else {
            res.redirect(302, "/");
        }
    });
});


userRoutes.post("/update-avatar", upload.single("image"), (req, res) => {
    authenticateUser(req).then(() => {
        if (!req.file) {
            res.status(400).send({
                error: new Errors.ValidationError("Could not find uploaded file")
            });
            return;
        }

        if (req.file.size > process.env.MAX_AVATAR_SIZE) {
            res.status(400).send({
                error: new Errors.ValidationError("Max file size is " + formatBytes(process.env.MAX_AVATAR_SIZE))
            });
            return;
        }

        fileHandler.mkDir(BASE_LOCAL_PATH + "/avatars", (err) => {
            if (err) {
                console.error(`${err}\nSomething happened while trying to initialize the avatars folder`);
                res.status(500).send({
                    error: new Errors.ProcessingError()
                });
                return;
            }

            let base_avatar_subpath = "/avatars/" + req.file.filename + "." + req.file.mimetype.split("/")[1];

            fileHandler.copy(req.file.path, BASE_LOCAL_PATH + base_avatar_subpath, async (err) => {
                if (err) {
                    console.error(`${err}\nFailed to copy uploaded avatar to its correct folder`);
                    res.status(500).send({
                        error: new Errors.ProcessingError()
                    });
                    return;
                }

                if ((dirname(req.session.userInfo.avatar_url) + "/") !== DEFAULT_AVATAR_URL) {
                    // Delete the old avatar file
                    fileHandler.delete("./" + req.session.userInfo.avatar_url, () => { });
                }

                let avatar_url = BASE_PUBLIC_PATH + base_avatar_subpath;

                user_collection.updateOne({ username: req.session.userInfo.username }, {
                    $set: {
                        avatar_url: avatar_url
                    }
                });

                track_collection.updateMany({ "comments.user._id" : getCurrentUserId(req) }, {
                    $set: {
                        "comments.$[elem].user.avatar_url": avatar_url,
                    }
                }, {
                    arrayFilters: [ { "elem.user._id": getCurrentUserId(req) }]
                });

                track_collection.updateMany({ "poster._id" : getCurrentUserId(req) },{
                    $set: {
                        "poster.avatar_url" : avatar_url
                    }
                });

                req.session.userInfo.avatar_url = avatar_url;

                res.redirect(302, "/user/" + req.session.userInfo.username);
            });
        });
    }).catch(() => {
        res.status(401).send({
            error: new Errors.ValidationError("Unauthenticated request")
        });
    });
});

userRoutes.post("/update-background", upload.single("image"), (req, res) => {
    if (req.session.userInfo) {
        if (!req.file) {
            res.status(400).send({
                error: "Could not process your new background file"
            });
            return;
        }

        fileHandler.mkDir(BASE_LOCAL_PATH + "/custom_backgrounds", (err) => {
            if (err) {
                console.error(`${err}\nSomething happened while trying to initialize the custom backgrounds folder`);
                res.status(500).end();
                return;
            }

            let base_subpath = "/custom_backgrounds/" + req.file.filename + "." + req.file.mimetype.split("/")[1];

            fileHandler.copy(req.file.path, BASE_LOCAL_PATH + base_subpath, (err) => {
                if (err) {
                    console.error(`${err}\nFailed to copy uploaded custom background to its correct folder`);
                    res.status(500).end();
                    return;
                }

                if (req.session.userInfo.custom_background_url) {
                    // Delete the old file
                    fileHandler.delete("./" + req.session.userInfo.custom_background_url, () => { });
                }

                user_collection.updateOne({ username: req.session.userInfo.username }, {
                    $set: {
                        custom_background_url: BASE_PUBLIC_PATH + base_subpath
                    }
                });

                req.session.userInfo.custom_background_url = BASE_PUBLIC_PATH + base_subpath;

                res.redirect(302, "/user/" + req.session.userInfo.username);
            });
        });
    } else {
        res.status(401).send({
            error: "unauthenticated request"
        });
    }
});

userRoutes.get("/info/:username", (req, res) => {
    authenticate(req, () => {
        getUserWithUsername(req.params.username).then((profile_info) => {
            if (profile_info) {
                res.send({
                    error: null,
                    info: profile_info
                });
            } else {
                res.send({
                    error: "user does not exist",
                    info: null
                });
            }
        });
    }, () => {
        res.send({
            error: new Errors.ValidationError("Unauthenticated request")
        });
    });
});

userRoutes.put("/password/:username", (req, res) => {
    if (req.session.userInfo && req.session.userInfo.username === req.params.username) {
        if (req.body && req.body.password) {
            if (req.body.password.length < 5) {
                res.status(400).send({
                    error: "Password is too short"
                });
                return;
            }

            bcrypt.genSalt(SALT_ROUNDS, (err, salt) => {
                bcrypt.hash(req.body.password, salt, (err, hash) => {
                    user_collection.find({ username: req.params.username }).toArray((err, result) => {
                        if (result.length === 0) {
                            res.send({
                                error: `User ${req.body.username} don't exist`
                            });
                        } else {
                            user_collection.updateOne({ username: req.params.username }, {
                                $set: {
                                    password: hash
                                }
                            });
                            res.send({
                                error: null
                            });
                        }
                    });
                });
            });
        } else {
            res.status(400).send({
                error: "invalid request"
            });
        }
    } else {
        res.status(401).end();
    }
});

userRoutes.put("/:username", (req, res) => {
    if (req.session.userInfo && req.session.userInfo.username === req.params.username) {
        if (req.body) {
            Object.keys(req.body).forEach(val => {
                console.log("Updating key: " + val + " for user: " + req.params.username);
                user_collection.updateOne({ username: req.params.username }, {
                    $set: {
                        [val]: req.body[val]
                    }
                });

                req.session.userInfo[val] = req.body[val];
            });

            res.send({
                error: null
            });
        } else {
            res.status(400).send({
                error: "invalid request"
            });
        }
    } else {
        res.status(401).end();
    }
});

userRoutes.get("/:username/favorites", (req, res) => {
    getUserWithUsername(req.params.username).then((profile_info) => {
        if (profile_info) {
            let bg_res;
            if (profile_info.custom_background_url) {
                bg_res = {
                    path: profile_info.custom_background_url,
                    game_name: null,
                    artist_name: null
                };
            } else {
                bg_res = getWallpaper();
            }

            res.render("favorites", {
                meta_title: process.env.APP_NAME,
                bg_src: bg_res.path,
                bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
                bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
                profile_info: profile_info,
				bg_art_source: bg_res.source,
                user_info: req.session.userInfo,
                is_current_user: req.session.userInfo ? profile_info._id === req.session.userInfo._id : false,
            });
        } else {
            res.render("404", {
                meta_title: process.env.APP_NAME,
                bg_src: BG_INFO_404.path,
                bg_game_name: BG_INFO_404.game_name,
				bg_art_source: BG_INFO_404.source,
                bg_artist_name: BG_INFO_404.artist_name,
                user_info: req.session.userInfo,
            });
        }
    }).catch((err) => {
        console.error(err, "Could not get favorite page for user " + req.params.username);
        res.render("404", {
            meta_title: process.env.APP_NAME,
            bg_src: BG_INFO_404.path,
            bg_game_name: BG_INFO_404.game_name,
            bg_artist_name: BG_INFO_404.artist_name,
            bg_art_source: BG_INFO_404.source,
            user_info: req.session.userInfo,
        });
    });
});

userRoutes.get("/:username", (req, res) => {
    getUserWithUsername(req.params.username).then((profile_info) => {
        if (profile_info) {
            let bg_res;
            if (profile_info.custom_background_url) {
                bg_res = {
                    path: profile_info.custom_background_url,
                    game_name: null,
                    artist_name: null
                };
            } else {
                bg_res = getWallpaper();
            }

            let listening_to_info;
            {
                let data = getListenersInfo();
                let result = data.find(user => user.username === profile_info.username);
                if (result && result.listening_to) listening_to_info = result.listening_to;
            }

            res.render("profile", {
                meta_title: process.env.APP_NAME,
                bg_src: bg_res.path,
                bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
                bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
				bg_art_source: bg_res.source,
                profile_info: profile_info,
                user_info: req.session.userInfo,
                is_current_user: req.session.userInfo ? profile_info.username === req.session.userInfo.username : false,
                listening_to: listening_to_info,
                max_avatar_size: process.env.MAX_AVATAR_SIZE
            });
        } else {
            res.render("404", {
                meta_title: process.env.APP_NAME,
                bg_src: BG_INFO_404.path,
                bg_game_name: BG_INFO_404.game_name,
                bg_artist_name: BG_INFO_404.artist_name,
				bg_art_source: BG_INFO_404.source,
                user_info: req.session.userInfo,
            });
        }
    }).catch((err) => {
        console.error(err);
        res.render("404", {
            meta_title: process.env.APP_NAME,
            bg_src: BG_INFO_404.path,
            bg_game_name: BG_INFO_404.game_name,
            bg_artist_name: BG_INFO_404.artist_name,
            user_info: req.session.userInfo,
            bg_art_source: BG_INFO_404.source
        });
    });
});

export function updateUser(id, key, data, update_operator) {
    return new Promise((resolve, reject) => {
        user_collection.updateOne({ _id: mongodb.ObjectId(id) }, {
            [update_operator]: {
                [key]: data
            }
        }).then(() => {
            resolve();
        }).catch(err => {
            reject(new Errors.ProcessingError(err));
        });
    });
}

export function getUserById(id, projection = { password: 0 }) {
    return new Promise((resolve, reject) => {
        user_collection.findOne({
            _id: mongodb.ObjectId(id),
        }, {
            "projection": projection
        }).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
}

export async function getMicroUser(id) {
    let user_info = await getUserById(id);
    return new MicroUser(id, user_info.username, user_info.avatar_url);
}

function _clearPrivateFields(user_info) {
    let tmp = user_info;
    delete tmp.password;
    delete tmp.email;
    return tmp;
}

export function getUserWithUsername(username) {
    return new Promise((resolve, reject) => {
        user_collection.findOne({
            username: username
        }).then(result => {
            if (result) {
                resolve(_clearPrivateFields(result));
            } else {
                resolve(null);
            }
        }).catch(err => {
            if (err) {
                console.error(`${err}\nAn error happened while trying to get info for user: ${username}`);
                reject(err);
                return;
            }
        });
    });
}

export function getCurrentUserId(req) {
    return req.session.userInfo._id;
}

function createUserSession(req, user, remember_me) {
    req.session.userInfo = user;

    if (remember_me) {
        req.session.cookie.maxAge = 31536000000; // Cookie expires after a year
    } else {
        // the session will expire when the site's closed
        req.session.cookie.expires = false;
    }
}

export function checkLogin(username, password) {
    return new Promise((resolve, _reject) => {
        user_collection.findOne({ username: username }).then(result => {
            if (result) {
                bcrypt.compare(password, result.password, (err, compare_result) => {
                    if (compare_result) {
                        resolve(result);
                    } else {
                        resolve(false);
                    }
                });
            } else {
                resolve(false);
            }
        }).catch(err => {
            resolve(false);
        });
    });
}

export async function getUserUploadsCount(user_id) {
	return (await user_collection.findOne({
		_id: mongodb.ObjectId(user_id)
	})).uploads.length;
}

export async function getUserFavoritesCount(user_id) {
	return (await user_collection.findOne({
		_id: mongodb.ObjectId(user_id)
	})).favorites.length;
}


export async function getUserFavorites(user_id) {
    return (await user_collection.aggregate([
            {
                $match: {
                    _id: mongodb.ObjectId(user_id),
                }
            },
            {
                $lookup: {
                    from: TRACK_COLLECTION_NAME,
                    localField: "favorites",
                    foreignField: "_id",
                    as: "favorites"
                }
            }
    ]).next()).favorites;
}


export async function getUserUploads(user_id) {
    return (await user_collection.aggregate([
        {
            $match: {
                _id: mongodb.ObjectId(user_id),
            }
        },
        {
            $lookup: {
                from: TRACK_COLLECTION_NAME,
                localField: "uploads",
                foreignField: "_id",
                as: "uploads"
            }
        }
    ]).next()).uploads;
}
