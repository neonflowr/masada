import express from "express";

import { authenticate } from "../func/auth_helper.js";

import { getPopularSongsInfo } from "../func/chat.js";

const router = express.Router();

router.get("/homepage", (req, res) => {
	authenticate(req, async () => {
		try {
			const data = await getPopularSongsInfo();

			res.send({
				items: data
			});
		} catch (err) {
			console.error(`${err}\nCould not process request for homepage listeners data`);
			res.status(500).end();
		}
	}, () => {
		res.status(401).end();
	});
});


router.get("/all", (req, res) => {
	authenticate(req, async () => {
		try {
			res.send(await getPopularSongsInfo());
		} catch(err) {
			console.error(`${err}\nCould not process request for all listeners data`);
			res.status(500).end();
		}
	}, () => {
		res.status(401).end();
	});
});

export default router;
