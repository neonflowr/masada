import { Server } from "socket.io";
import { getTrackById } from "../routes/track.js";
import { getRangedRandomIndex } from "./utils.js";

let users_list = [];
let popular_songs = {};

let all_sockets = {};

export function getListenersInfo() {
    return users_list;
}

export function getPopularSongsInfo() {
    return new Promise((resolve) => {
        let songs_with_more_than_0_listeners = {};
        Object.keys(popular_songs).forEach(song => {
            if (popular_songs[song].listeners.length > 0) songs_with_more_than_0_listeners[song] = popular_songs[song];
        });

        if(Object.keys(songs_with_more_than_0_listeners).length === 0) {
            resolve({});
            return;
        }
        Object.keys(songs_with_more_than_0_listeners).forEach(async (val, index) => {
            let data = await getTrackById(songs_with_more_than_0_listeners[val]._id);
            let tmp = songs_with_more_than_0_listeners[val].listeners;
            songs_with_more_than_0_listeners[val] = data;
            songs_with_more_than_0_listeners[val].listeners = tmp;

            if (index === Object.keys(songs_with_more_than_0_listeners).length -1) resolve(songs_with_more_than_0_listeners);
        });
    });
}

export function startWebSocketServer(server) {
    const io = new Server(server, {});

    io.on("connection", (socket) => {
        let ip = socket.request.headers["x-forwarded-for"] || socket.request.connection.remoteAddress;

        initSocketConnection();

        socket.on("join", (user_info) => {
            try {
                if (!isSocketLoggedIn()) {
                    console.log("User " + user_info.username + " just joined");

                    if (Object.keys(all_sockets[ip].user).length !== 0 && all_sockets[ip].socket_id !== []) {
                        // There are already previous socket connections
                        let temp = all_sockets[ip].user.listening_to;
                        all_sockets[ip].user = user_info;
                        all_sockets[ip].user.listening_to = temp;
                        users_list.push(user_info);

                        // Set this socket connection as the first client
                        let index = all_sockets[ip].socket_id.indexOf(socket.id);
                        all_sockets[ip].socket_id.splice(index, 1);
                        all_sockets[ip].socket_id.unshift(socket.id);
                    } else {
                        all_sockets[ip].user = user_info;
                        users_list.push(user_info);
                    }

                    io.emit("update-user-list", users_list);

                    io.emit("update-listener-count", {
                        all: Object.keys(all_sockets).length,
                        anonymous: Object.keys(all_sockets).length - users_list.length
                    });

                    io.emit("message", {
                        type: "system",
                        username: "",
                        timestamp: "",
                        data: user_info.username,
                        event: "user_join"
                    });
                } else {
                    // Rejoin
                    socket.emit("update-user-list", users_list);

                    socket.emit("update-listener-count", {
                        all: Object.keys(all_sockets).length,
                        anonymous: Object.keys(all_sockets).length - users_list.length
                    });
                }
            } catch (err) {
                console.error(`${err}\nCould not process join request`);
            }
        });

        socket.on("leave", () => {
            leaveRoom("leave");
        });

        socket.on("disconnect", () => {
            handleDisconnect();
        });

        socket.on("message", (info) => {
            try {
                if (all_sockets[ip] && all_sockets[ip].socket_id.includes(socket.id)) {
                    if (info.type === "system" && !isFirstClient()) return;

                    io.emit("message", {
                        type: info.type,
                        user: info.user,
                        event: info.event,
                        message: info.message,
                        data: info.data,
                    });
                }
            } catch (err) {
                console.error(`${err}\nCould not process message`);
            }
        });

        socket.on("track-update", (info) => {
            try {
                if (isFirstClient()) { // Only take updates from the first socket connection
                    if (!info.listening_to || Object.keys(info.listening_to).length === 0) return;

                    // let unupdated_user_info = users_list.find(user => user.username === info.user.username);
                    let unupdated_user_info = all_sockets[ip].user;

                    if (popular_songs[info.listening_to._id] === undefined) {
                        popular_songs[info.listening_to._id] = {
                            _id: info.listening_to._id,
                            name: info.listening_to.name,
                            path: info.listening_to.path,
                            listeners: []
                        };
                    } else {
                        // Update the stored info
                        popular_songs[info.listening_to._id].name = info.listening_to.name;
                        popular_songs[info.listening_to._id].path = info.listening_to.path;
                    }

                    // Increment the count for the received song
                    if (!popular_songs[info.listening_to._id].listeners.find(item => item === ip)) {
                        popular_songs[info.listening_to._id].listeners.push(ip);
                    }

                    // Decrement the count for the user's previous song
                    if (unupdated_user_info.listening_to && Object.keys(popular_songs).includes(unupdated_user_info.listening_to._id) && unupdated_user_info.listening_to._id !== info.listening_to._id) {
                        let find = popular_songs[unupdated_user_info.listening_to._id].listeners.find(item => item === ip);
                        if (find) {
                            let index = popular_songs[unupdated_user_info.listening_to._id].listeners.indexOf(find);
                            popular_songs[unupdated_user_info.listening_to._id].listeners.splice(index, 1);
                        }
                    }

                    // update
                    unupdated_user_info.listening_to = info.listening_to;

                    // See if the current socket is logged in
                    let user_info;
                    user_info = users_list.find(user => user.username === info.user.username);
                    if (user_info) user_info.listening_to = info.listening_to;

                    if (user_info) {
                        io.emit("message", {
                            type: "system",
                            user: info.user,
                            message: info.message,
                            event: "track_update",
                            data: {
                                user: info.user,
                                track: info.listening_to,
                            }
                        });

                        io.emit("update-user-list", users_list);
                    }

                    io.emit("update-listener-count", {
                        all: Object.keys(all_sockets).length,
                        anonymous: Object.keys(all_sockets).length - users_list.length
                    });

                    io.emit("update-popular-songs", determinePopularSong(popular_songs));
                } else if (all_sockets[ip] && all_sockets[ip].socket_id[0] !== socket.id) {
                    // Same user but different socket connection
                    socket.emit("update-listener-count", {
                        all: Object.keys(all_sockets).length,
                        anonymous: Object.keys(all_sockets).length - users_list.length
                    });

                    socket.emit("update-popular-songs", determinePopularSong(popular_songs));

                    socket.emit("update-user-list", users_list);
                }
            } catch (err) {
                console.error(`${err}\nCould not process message`);
            }
        });

        // Only the first socket connection's actions are acted on
        function isFirstClient() {
            return all_sockets[ip] && all_sockets[ip].socket_id[0] === socket.id;
        }

        function leaveRoom() {
            if (isFirstClient() && isSocketLoggedIn()) {
                users_list.splice(users_list.indexOf(all_sockets[ip].user), 1);

                // reset stored user info
                let temp = all_sockets[ip].user.listening_to;
                all_sockets[ip].user = {
                    listening_to: temp
                };

                io.emit("update-user-list", users_list);
                io.emit("update-listener-count", {
                    all: Object.keys(all_sockets).length,
                    anonymous: Object.keys(all_sockets).length - users_list.length
                });

                if (Object.keys(popular_songs).length > 0) io.emit("update-popular-songs", determinePopularSong(popular_songs));

                socket.emit("leave-success");
            } else {
                socket.emit("leave-fail");
            }
        }

        function initSocketConnection() {
            if (process.env.NODE_ENV === "test") {
                // Randomize for testing
                ip += getRangedRandomIndex(1000);
            }

            if (Object.keys(all_sockets).indexOf(ip) === -1) {
                all_sockets[ip] = {
                    user: {
                        listening_to: {
                            name: undefined
                        }
                    },
                    socket_id: [socket.id]
                };
            } else {
                if (!all_sockets[ip].socket_id.includes(socket.id)) {
                    all_sockets[ip].socket_id.push(socket.id);
                }
            }

            socket.emit("update-listener-count", {
                all: Object.keys(all_sockets).length,
                anonymous: Object.keys(all_sockets).length - users_list.length
            });

            if (Object.keys(popular_songs).length > 0) socket.emit("update-popular-songs", determinePopularSong(popular_songs));

            socket.emit("update-user-list", users_list);
        }

        function determinePopularSong(popular_songs) {
            let max_counter = 0;
            let equal_listener_songs = [];

            Object.keys(popular_songs).map((_id) => {
                let current_song = popular_songs[_id];

                if (current_song.listeners.length > max_counter) {
                    max_counter = current_song.listeners.length;
                    equal_listener_songs = [];
                    equal_listener_songs.push({ info: current_song, id: _id });
                } else if (current_song.listeners.length === max_counter) {
                    equal_listener_songs.push({ info: current_song, id: _id });
                }
            });

            let randomized_popular_song = equal_listener_songs[Math.floor(Math.random() * equal_listener_songs.length)];
            randomized_popular_song = popular_songs[randomized_popular_song.id];

            return randomized_popular_song;
        }

        function isSocketLoggedIn() {
            let val = Object.keys(all_sockets[ip].user).includes("username");
            return val;
        }

        function handleDisconnect() {
            try {
                console.log("handling socket disconnect");
                if (all_sockets[ip].socket_id.includes(socket.id)) {
                    if(isFirstClient()) {
                        // Decrement the count for the user's current song
                        let index = popular_songs[all_sockets[ip].user.listening_to._id].listeners.indexOf(ip);
                        if (index !== undefined && index !== -1) {
                            popular_songs[all_sockets[ip].user.listening_to._id].listeners.splice(index);
                        }
                    }

                    all_sockets[ip].socket_id.splice(all_sockets[ip].socket_id.indexOf(socket.id), 1);

                    if (all_sockets[ip].socket_id.length === 0) {
                        if (isSocketLoggedIn()) {
                            io.emit("message", {
                                type: "system",
                                message: `${all_sockets[ip].user.username} just left`,
                                username: "",
                                timestamp: ""
                            });

                            users_list.splice(users_list.indexOf(all_sockets[ip].user), 1);

                            console.log("a listener has fully left");
                            delete all_sockets[ip];

                            io.emit("update-user-list", users_list);
                            io.emit("update-listener-count", {
                                all: Object.keys(all_sockets).length,
                                anonymous: Object.keys(all_sockets).length - users_list.length
                            });

                            if (Object.keys(popular_songs).length > 0) io.emit("update-popular-songs", determinePopularSong(popular_songs));

                            return;
                        }

                        console.log("a listener has fully left");
                        delete all_sockets[ip];
                    }
                } else {
                    console.error("Exit request invalid");
                }
            } catch (err) {
                console.error(`${err}\ncould not process disconnect signal`);
                delete all_sockets[ip];
            }
        }
    });
}
