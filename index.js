import express from "express";
import bodyParser from "body-parser";
import http from "http";

import sessions from "express-session";
import cookieParser from "cookie-parser";
import FileStore from "session-file-store";

import { BASE_LOCAL_PATH, fileHandler } from "./func/file.js";
import { i18n } from "./func/localization.js";
import { imageRoutes, getWallpaper, BG_INFO_404, getLimitedRandomBG, USE_SZURUBOORU_CONTENT } from "./routes/image.js";
import listenerRoutes from "./routes/listener.js";
import { userRoutes, getCurrentUserId, updateUser} from "./routes/user.js";
import { trackRoutes, getTrackById, getRandomTrack } from "./routes/track.js";

import { startWebSocketServer } from "./func/chat.js";


import { replaceUnderscoreWithSpace } from "./func/utils.js";

import morgan from "morgan";

const app = express();

const server = http.createServer(app);

const PORT = process.env.PORT ?? 3000;

// Setup the storage place
if (!fileHandler.existsSync(BASE_LOCAL_PATH)) {
    fileHandler.mkDir(BASE_LOCAL_PATH, (err) => {
        if (err) {
            console.error(`${err}\nFailed to initialize storage space for the application...`);
            return;
        }
    });
}

// Middlewares
app.use(bodyParser.json());
app.use(cookieParser());
app.use(sessions({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: true,
    cookie: {
        maxAge: 31536000000, // Cookie expires after one year
        sameSite: "lax",
        secure: process.env.NODE_ENV !== "test",
        httpOnly: true
    },
    resave: false,
    store: new (FileStore(sessions))({})
}));

// Serve static files from these folders
app.use(express.static("public"));
app.use("/data", express.static("data"));

// HACK: Declare this below the static declarations so Morgan will only log route requests and not assets requests
app.use(morgan(process.env.NODE_ENV === "test" ? "dev" : "short"));

app.use(i18n.init);
app.use(async (req, res, next) => {
    if (req.cookies && req.cookies.language) {
        i18n.setLocale(req, req.cookies.language);
    }
    if (req.session.userInfo) {
        updateUser(getCurrentUserId(req), "last_seen", new Date(), "$set");
    }

    next();
});

app.use("/image", imageRoutes);
app.use("/listener", listenerRoutes);
app.use("/track", trackRoutes);
app.use("/user", userRoutes);

// So we can get accurate remote IP addresses
app.set("trust proxy", true);

app.set("view engine", "pug");

app.get("/", async (req, res) => {
    try {
        if (req.query.track_id && req.query.track_id !== "") {
            let result = await getTrackById(req.query.track_id);
            if (result) {
                res.render("main", {
                    meta_title: `${result.name} / ${process.env.APP_NAME}`,
                    meta_desc: `Listen to ${result.name} on ${process.env.APP_NAME}`,
                    meta_url: `${process.env.APP_BASE_URL}/?track_id=${req.query.track_id}`,
                    meta_img: result.img_src_wide,
                    meta_sitename: process.env.APP_NAME,
                    user_info: req.session.userInfo,
                    use_szurubooru_content: USE_SZURUBOORU_CONTENT,
                });
            } else {
                res.render("404", {
                    meta_title: process.env.APP_NAME,
                    bg_src: BG_INFO_404.path,
                    bg_game_name: replaceUnderscoreWithSpace(BG_INFO_404.game_name),
                    bg_artist_name: replaceUnderscoreWithSpace(BG_INFO_404.artist_name),
                    user_info: req.session.userInfo,
                    bg_art_source: BG_INFO_404.source
                });
            }
        } else {
            let wallpaper = getWallpaper();

            let banner = USE_SZURUBOORU_CONTENT ? getLimitedRandomBG() : null;
            let random_track_info = (await getRandomTrack())[0];

            res.render("homepage", {
                meta_title: process.env.APP_NAME,
                meta_desc: process.env.APP_TAGLINE,
                meta_img: "/assets/icons/preview.png",
                meta_url: process.env.APP_BASE_URL,
                bg_src: wallpaper.path,
                banner: banner,
                random_track_info: random_track_info,
                user_info: req.session.userInfo,
            });
        }
    } catch (err) {
        console.error(err, "Could not process home page request");
        res.render("404", {
            meta_title: process.env.APP_NAME,
            bg_src: BG_INFO_404.path,
            bg_game_name: replaceUnderscoreWithSpace(BG_INFO_404.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(BG_INFO_404.artist_name),
            bg_art_source: BG_INFO_404.source,
            user_info: req.session.userInfo,
        });
    }
});

app.get("/upload", (req, res) => {
    try {
        if (!req.session.userInfo) {
            res.render("404", {
                meta_title: process.env.APP_NAME,
                bg_src: BG_INFO_404.path,
                bg_game_name: replaceUnderscoreWithSpace(BG_INFO_404.game_name),
                bg_art_source: BG_INFO_404.source,
                bg_artist_name: replaceUnderscoreWithSpace(BG_INFO_404.artist_name),
                user_info: req.session.userInfo,
            });
        } else {
            let bg_res = getWallpaper();

            res.render("upload", {
                meta_title: process.env.APP_NAME,
                bg_src: bg_res.path,
                bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
                bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
                bg_art_source: bg_res.source,
                user_info: req.session.userInfo
            });
        }
    } catch (err) {
        console.error(`${err}\nCould not process upload page request`);
        res.status(500).end();
    }
});

app.get("/comments", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("comments", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            bg_art_source: bg_res.source,
            user_info: req.session.userInfo
        });
    } catch (err) {
        console.error(`${err}\nCould not process users page request`);
        res.status(500).end();
    }
});

app.get("/users", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("users", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_art_source: bg_res.source,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            user_info: req.session.userInfo
        });
    } catch (err) {
        console.error(`${err}\nCould not process users page request`);
        res.status(500).end();
    }
});


app.get("/about", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("about", {
            meta_title: process.env.APP_NAME,
            meta_desc: process.env.APP_TAGLINE,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_art_source: bg_res.source,
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            user_info: req.session.userInfo,
            app_version: process.env.APP_VERSION,
            booru_info: {
                name: process.env.BOORU_NAME,
                url: process.env.BOORU_URL
            },
            discord_link: process.env.DISCORD_LINK
        });
    } catch (err) {
        console.error(`${err}\nCould not process about page request`);
        res.status(500).end();
    }
});

app.get("/hotkeys", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("hotkeys", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_art_source: bg_res.source,
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            user_info: req.session.userInfo
        });
    } catch (err) {
        console.error(`${err}\nCould not process about page request`);
        res.status(500).end();
    }
});

app.get("/login", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("login", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            bg_art_source: bg_res.source,
            user_info: req.session.userInfo
        });
    } catch (err) {
        console.error(`${err}\nCould not process login page request`);
        res.status(500).end();
    }
});

app.get("/register", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("register", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            bg_art_source: bg_res.source,
            user_info: req.session.userInfo
        });
    } catch (err) {
        console.error(`${err}\nCould not process register page request`);
        res.status(500).end();
    }
});

app.get("/preferences", (req, res) => {
    try {
        let bg_res = getWallpaper();

        res.render("preferences", {
            meta_title: process.env.APP_NAME,
            bg_src: bg_res.path,
            bg_game_name: replaceUnderscoreWithSpace(bg_res.game_name),
            bg_artist_name: replaceUnderscoreWithSpace(bg_res.artist_name),
            bg_art_source: bg_res.source,
            user_info: req.session.userInfo,
            lang: req.session.language ?? "en"
        });
    } catch (err) {
        console.error(`${err}\nCould not process register page request`);
        res.status(500).end();
    }
});

// 404
app.get("*", function(req, res) {
    res.status(404).render("404", {
        meta_title: process.env.APP_NAME,
        bg_src: BG_INFO_404.path,
        bg_game_name: replaceUnderscoreWithSpace(BG_INFO_404.game_name),
        bg_artist_name: replaceUnderscoreWithSpace(BG_INFO_404.artist_name),
        user_info: req.session.userInfo,
        bg_art_source: BG_INFO_404.source,
    });
});

app.post("/lang", (req, res) => {
    res.cookie("language", req.body.language);
    res.status(200).end();
});

// Start listening to requests
server.listen(PORT, () => {
    console.log("Server started");
});

startWebSocketServer(server);
