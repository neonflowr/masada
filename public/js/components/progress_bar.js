const progressBarComponent = {
    props: {
        status: {
            type: Number,
            required: true,
            default: 25,
            validator(value) {
                return Number.isSafeInteger(value);
            }
        }
    },
    data() {
        local_status: status // eslint-disable-line
    },
    template: `
        <div class="progress position-fixed w-100 top-0 start-50 border-0 translate-middle-x" style="height: .2rem;">
            <div ref="bar" class="progress-bar" role="progressbar" :class="{ 'w-25': local_status===25, 'w-100': local_status===100, 'w-50': local_status===50 }" style="opacity:0;"/>
        </div> 
    `,
    created() {
        this.local_status = this.status;
    },
    methods: {
        setStartedStatus() {
            this.local_status = 25;
            this.$refs.bar.style.opacity = 1;
            this.$forceUpdate();
        },

        setFinishedStatus(hide_on_finished=true) {
            this.local_status = 100;
            this.$forceUpdate();

            if (hide_on_finished) {
                setTimeout(() => {
                    this.hide();
                }, 500);
            }
        },

        hide() {
            this.$refs.bar.style.opacity = 0;
            this.$forceUpdate();
        }
    }
};

export default progressBarComponent;
