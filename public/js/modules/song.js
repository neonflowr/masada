import * as HttpHandler from "./http.js";

const MAX_RETRY_COUNT = 10;
let retry_count = 0;

export function getPagedSongsInfo(offset = 0, size = 42) {
	return new Promise((resolve, reject) => {
		_getPagedData(resolve, reject, offset, size);
	});
}

export function getAllSongsInfo() {
    return new Promise((resolve, reject) => {
        _getData(resolve, reject);
    });
}

export function addToFavorite(info, callback) {
    HttpHandler.postAsync(`${HttpHandler.BASE_URL}/track/${info._id}/favorite`, {}, async (res) => {
        callback(await res.json());
    });
}

function _getPagedData(resolve, reject, offset, size) {
	HttpHandler.getAsync(HttpHandler.GET_PAGED_TRACK_INFO_URL + `?offset=${offset}&?size=${size}`).then(response => {
		if (response) {
			if (response.status === 200) {
				resolve(JSON.parse(response.responseText));
			} else {
				if (retry_count > MAX_RETRY_COUNT) {
					reject(response);
				} else {
					setTimeout(() => {
						retry_count++;
						_getPagedData(resolve, reject, offset, size);
					}, 3000);
				}
			}
		}
	});
}

function _getData(resolve, reject) {
    HttpHandler.getAsync(HttpHandler.GET_ALL_TRACK_INFO_URL).then(response => {
        if (response) {
            if (response.status === 200) {
                resolve(JSON.parse(response.responseText));
            } else {
                if (retry_count > MAX_RETRY_COUNT) {
                    reject(response);
                } else {
                    setTimeout(() => {
                        retry_count++;
                        _getData(resolve, reject);
                    }, 3000);
                }
            }
        }
    });
}
